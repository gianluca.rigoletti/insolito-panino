/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.l = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };

/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};

/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};

/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

eval("var toggleNavbar = false;\nvar header = document.querySelectorAll('header')[0];\nvar navbar = document.querySelectorAll('nav')[1];\nvar chevron = document.querySelectorAll('.chevron')[0];\n\n/**\n * Parte di ordinamento\n */\n/*const foods = [...document.querySelectorAll('.food-item')];\nconst menuFoods = document.querySelectorAll('.main-menu')[0];\n\nfunction compareFoods(a, b) {\n    let aPrice = a.getAttribute('data-food-price');\n    let bPrice = b.getAttribute('data-food-price');\n    if (aPrice < bPrice) return -1;\n    if (aPrice === bPrice) return 0;\n    if (aPrice > bPrice) return 1;\n}\nconst arrayFoods = foods.sort(compareFoods);\nfor (var i = 0; i < foods.length; ++i) {\n    console.log(arrayFoods[0][i]);\n    menuFoods.appendChild(arrayFoods[0][i]);\n}*/\n\nheader.addEventListener('click', function () {\n    toggleNavbar = !toggleNavbar;\n    if (toggleNavbar) {\n        navbar.classList.remove('navbar-hidden');\n        navbar.classList.add('navbar-visible');\n        chevron.classList.remove('bottom');\n        chevron.classList.add('top');\n    } else {\n        navbar.classList.remove('navbar-visible');\n        navbar.classList.add('navbar-hidden');\n        chevron.classList.add('bottom');\n        chevron.classList.remove('top');\n    }\n});\n\n/**\n * Custom modal plugin\n */\nvar mainInfo = document.querySelectorAll('.main-info');\nif (mainInfo.length !== 0) {\n    var places = document.querySelectorAll('.table-info-place');\n    // Devo fare il loop su tutti gli elementi del nodelist per aggiungere \n    // l'event handler. Uso ES6\n    [].forEach.call(places, function (place) { return place.addEventListener('click', handleModal, false); });\n}\n\nfunction handleModal(event) {\n    // Scurisco il dark frame\n    var modal = document.querySelector('.modal-info');\n    var frame = document.querySelector('.modal-dark-frame');\n    frame.style.opacity = '0.4';\n    modal.style.zIndex = 2;\n    frame.style.zIndex = 1;\n    modal.classList.remove('modal-info-hidden');\n    modal.classList.add('modal-info-visible');\n\n    var modalCloseButtons = document.querySelectorAll('.modal-close');\n    [].forEach.call(modalCloseButtons, function (button) { return button.addEventListener('click', handleCloseModal, false); });\n\n}\n\nfunction handleCloseModal(event) {\n    var modal = document.querySelector('.modal-info');\n    modal.classList.add('modal-info-hidden');\n    modal.classList.remove('modal-info-visible');\n    modal.style.zIndex = null;\n    var frame = document.querySelector('.modal-dark-frame');\n    frame.style.opacity = null;\n    frame.style.zIndex = null;\n}//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9yZXNvdXJjZXMvYXNzZXRzL2pzL2FwcC5qcz84YjY3Il0sInNvdXJjZXNDb250ZW50IjpbInZhciB0b2dnbGVOYXZiYXIgPSBmYWxzZTtcbmNvbnN0IGhlYWRlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ2hlYWRlcicpWzBdO1xuY29uc3QgbmF2YmFyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnbmF2JylbMV07XG5jb25zdCBjaGV2cm9uID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmNoZXZyb24nKVswXTtcblxuLyoqXG4gKiBQYXJ0ZSBkaSBvcmRpbmFtZW50b1xuICovXG4vKmNvbnN0IGZvb2RzID0gWy4uLmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5mb29kLWl0ZW0nKV07XG5jb25zdCBtZW51Rm9vZHMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcubWFpbi1tZW51JylbMF07XG5cbmZ1bmN0aW9uIGNvbXBhcmVGb29kcyhhLCBiKSB7XG4gICAgbGV0IGFQcmljZSA9IGEuZ2V0QXR0cmlidXRlKCdkYXRhLWZvb2QtcHJpY2UnKTtcbiAgICBsZXQgYlByaWNlID0gYi5nZXRBdHRyaWJ1dGUoJ2RhdGEtZm9vZC1wcmljZScpO1xuICAgIGlmIChhUHJpY2UgPCBiUHJpY2UpIHJldHVybiAtMTtcbiAgICBpZiAoYVByaWNlID09PSBiUHJpY2UpIHJldHVybiAwO1xuICAgIGlmIChhUHJpY2UgPiBiUHJpY2UpIHJldHVybiAxO1xufVxuY29uc3QgYXJyYXlGb29kcyA9IGZvb2RzLnNvcnQoY29tcGFyZUZvb2RzKTtcbmZvciAodmFyIGkgPSAwOyBpIDwgZm9vZHMubGVuZ3RoOyArK2kpIHtcbiAgICBjb25zb2xlLmxvZyhhcnJheUZvb2RzWzBdW2ldKTtcbiAgICBtZW51Rm9vZHMuYXBwZW5kQ2hpbGQoYXJyYXlGb29kc1swXVtpXSk7XG59Ki9cblxuaGVhZGVyLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgIHRvZ2dsZU5hdmJhciA9ICF0b2dnbGVOYXZiYXI7XG4gICAgaWYgKHRvZ2dsZU5hdmJhcikge1xuICAgICAgICBuYXZiYXIuY2xhc3NMaXN0LnJlbW92ZSgnbmF2YmFyLWhpZGRlbicpO1xuICAgICAgICBuYXZiYXIuY2xhc3NMaXN0LmFkZCgnbmF2YmFyLXZpc2libGUnKTtcbiAgICAgICAgY2hldnJvbi5jbGFzc0xpc3QucmVtb3ZlKCdib3R0b20nKTtcbiAgICAgICAgY2hldnJvbi5jbGFzc0xpc3QuYWRkKCd0b3AnKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBuYXZiYXIuY2xhc3NMaXN0LnJlbW92ZSgnbmF2YmFyLXZpc2libGUnKTtcbiAgICAgICAgbmF2YmFyLmNsYXNzTGlzdC5hZGQoJ25hdmJhci1oaWRkZW4nKTtcbiAgICAgICAgY2hldnJvbi5jbGFzc0xpc3QuYWRkKCdib3R0b20nKTtcbiAgICAgICAgY2hldnJvbi5jbGFzc0xpc3QucmVtb3ZlKCd0b3AnKTtcbiAgICB9XG59KTtcblxuLyoqXG4gKiBDdXN0b20gbW9kYWwgcGx1Z2luXG4gKi9cbmNvbnN0IG1haW5JbmZvID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLm1haW4taW5mbycpO1xuaWYgKG1haW5JbmZvLmxlbmd0aCAhPT0gMCkge1xuICAgIGNvbnN0IHBsYWNlcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy50YWJsZS1pbmZvLXBsYWNlJyk7XG4gICAgLy8gRGV2byBmYXJlIGlsIGxvb3Agc3UgdHV0dGkgZ2xpIGVsZW1lbnRpIGRlbCBub2RlbGlzdCBwZXIgYWdnaXVuZ2VyZSBcbiAgICAvLyBsJ2V2ZW50IGhhbmRsZXIuIFVzbyBFUzZcbiAgICBbXS5mb3JFYWNoLmNhbGwocGxhY2VzLCBwbGFjZSA9PiBwbGFjZS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGhhbmRsZU1vZGFsLCBmYWxzZSkpO1xufVxuXG5mdW5jdGlvbiBoYW5kbGVNb2RhbChldmVudCkge1xuICAgIC8vIFNjdXJpc2NvIGlsIGRhcmsgZnJhbWVcbiAgICBjb25zdCBtb2RhbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5tb2RhbC1pbmZvJyk7XG4gICAgY29uc3QgZnJhbWUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubW9kYWwtZGFyay1mcmFtZScpO1xuICAgIGZyYW1lLnN0eWxlLm9wYWNpdHkgPSAnMC40JztcbiAgICBtb2RhbC5zdHlsZS56SW5kZXggPSAyO1xuICAgIGZyYW1lLnN0eWxlLnpJbmRleCA9IDE7XG4gICAgbW9kYWwuY2xhc3NMaXN0LnJlbW92ZSgnbW9kYWwtaW5mby1oaWRkZW4nKTtcbiAgICBtb2RhbC5jbGFzc0xpc3QuYWRkKCdtb2RhbC1pbmZvLXZpc2libGUnKTtcblxuICAgIGNvbnN0IG1vZGFsQ2xvc2VCdXR0b25zID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLm1vZGFsLWNsb3NlJyk7XG4gICAgW10uZm9yRWFjaC5jYWxsKG1vZGFsQ2xvc2VCdXR0b25zLCBidXR0b24gPT4gYnV0dG9uLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgaGFuZGxlQ2xvc2VNb2RhbCwgZmFsc2UpKTtcblxufVxuXG5mdW5jdGlvbiBoYW5kbGVDbG9zZU1vZGFsKGV2ZW50KSB7XG4gICAgY29uc3QgbW9kYWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubW9kYWwtaW5mbycpO1xuICAgIG1vZGFsLmNsYXNzTGlzdC5hZGQoJ21vZGFsLWluZm8taGlkZGVuJyk7XG4gICAgbW9kYWwuY2xhc3NMaXN0LnJlbW92ZSgnbW9kYWwtaW5mby12aXNpYmxlJyk7XG4gICAgbW9kYWwuc3R5bGUuekluZGV4ID0gbnVsbDtcbiAgICBjb25zdCBmcmFtZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5tb2RhbC1kYXJrLWZyYW1lJyk7XG4gICAgZnJhbWUuc3R5bGUub3BhY2l0eSA9IG51bGw7XG4gICAgZnJhbWUuc3R5bGUuekluZGV4ID0gbnVsbDtcbn1cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gcmVzb3VyY2VzL2Fzc2V0cy9qcy9hcHAuanMiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==");

/***/ }
/******/ ]);