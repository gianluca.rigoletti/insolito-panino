var toggleNavbar = false;
const header = document.querySelectorAll('header')[0];
const navbar = document.querySelectorAll('nav')[1];
const chevron = document.querySelectorAll('.chevron')[0];

/**
 * Parte di ordinamento
 */
/*const foods = [...document.querySelectorAll('.food-item')];
const menuFoods = document.querySelectorAll('.main-menu')[0];

function compareFoods(a, b) {
    let aPrice = a.getAttribute('data-food-price');
    let bPrice = b.getAttribute('data-food-price');
    if (aPrice < bPrice) return -1;
    if (aPrice === bPrice) return 0;
    if (aPrice > bPrice) return 1;
}
const arrayFoods = foods.sort(compareFoods);
for (var i = 0; i < foods.length; ++i) {
    console.log(arrayFoods[0][i]);
    menuFoods.appendChild(arrayFoods[0][i]);
}*/

header.addEventListener('click', function () {
    toggleNavbar = !toggleNavbar;
    if (toggleNavbar) {
        navbar.classList.remove('navbar-hidden');
        navbar.classList.add('navbar-visible');
        chevron.classList.remove('bottom');
        chevron.classList.add('top');
    } else {
        navbar.classList.remove('navbar-visible');
        navbar.classList.add('navbar-hidden');
        chevron.classList.add('bottom');
        chevron.classList.remove('top');
    }
});

/**
 * Custom modal plugin
 */
const mainInfo = document.querySelectorAll('.main-info');
if (mainInfo.length !== 0) {
    const places = document.querySelectorAll('.table-info-place');
    // Devo fare il loop su tutti gli elementi del nodelist per aggiungere 
    // l'event handler. Uso ES6
    [].forEach.call(places, place => place.addEventListener('click', handleModal, false));
}

function handleModal(event) {
    // Scurisco il dark frame
    const modal = document.querySelector('.modal-info');
    const frame = document.querySelector('.modal-dark-frame');
    frame.style.opacity = '0.4';
    modal.style.zIndex = 2;
    frame.style.zIndex = 1;
    modal.classList.remove('modal-info-hidden');
    modal.classList.add('modal-info-visible');

    const modalCloseButtons = document.querySelectorAll('.modal-close');
    [].forEach.call(modalCloseButtons, button => button.addEventListener('click', handleCloseModal, false));

}

function handleCloseModal(event) {
    const modal = document.querySelector('.modal-info');
    modal.classList.add('modal-info-hidden');
    modal.classList.remove('modal-info-visible');
    modal.style.zIndex = null;
    const frame = document.querySelector('.modal-dark-frame');
    frame.style.opacity = null;
    frame.style.zIndex = null;
}