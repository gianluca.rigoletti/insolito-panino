@extends('app')

@section('main')
    <main class="main main-contacts">
        <h1>Contatti</h1>
        <h4>Puoi contattarci attraverso:</h4>
        <section class="contacts-section contacts-whatsapp">
            <h5 class="contacts-header">Whatsapp</h5>
            <span class="contacts-image">📞</span>
            <a class="contacts-number" href="tel:+39123456789">+39 123456789</a>
        </section>
        <section class="contacts-section contacts-sms">
            <h5 class="contacts-header">Sms</h5>
            <span class="contacts-image contacts-image-mail">📧</span>
{{--             <img class="contacts-image" src="{{ elixir('/images/speech-bubble.svg') }}">
 --}}            <a class="contacts-number" href="tel:+39123456789">+39 123456789</a>
        </section>
        <section class="contacts-section contacts-phone">
            <h5 class="contacts-header">Telefono</h5>
            <img class="contacts-image" src="{{ elixir('/images/whatsapp.svg') }}">
            <a class="contacts-number" href="tel:+39123456789">+39 123456789</a>
        </section>
    </main>
@endsection