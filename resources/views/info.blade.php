@extends('app')

@section('main')
    <main class="main main-info">
        <h1>Orari e Luoghi</h1>
        <p class="paragraph-info">Puoi trovarci nei seguenti posti in questi giorni della settimana:</p>
        <table class="table table-info">
            <tr>
                <th>Giorno</th>
                <th class="table table-info table-info-orario">Orario</th>
                <th>Luogo</th>
            </tr>
            <tr>
                <td>Lunedì</td>
                <td class="table-info-hours">
                    <ul>
                        <li>12-14</li>
                        <li>18-20</li>
                    </ul>
                </td>
                <td>
                    <ul>
                        <li>
                            <a href="#mosciano" class="table table-info table-info-place">Mosciano</a>
                        </li>
                        <li>Pineto</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>Martedì</td>
                <td class="table-info-hours">
                    <ul>
                        <li>12-14</li>
                        <li>18-20</li>
                    </ul>
                </td>
                <td>
                    <ul>
                        <li><a href="#mosciano" class="table table-info table-info-place">Mosciano</a></li>
                        <li>Pineto</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>Mercoledì</td>
                <td class="table-info-hours">
                    <ul>
                        <li>12-14</li>
                        <li>18-20</li>
                    </ul>
                </td>
                <td>
                    <ul>
                        <li><a href="#mosciano" class="table table-info table-info-place">Mosciano</a></li>
                        <li>Pineto</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>Giovedì</td>
                <td class="table-info-hours">
                    <ul>
                        <li>12-14</li>
                        <li>18-20</li>
                    </ul>
                </td>
                <td>
                    <ul>
                        <li><a href="#mosciano" class="table table-info table-info-place">Mosciano</a></li>
                        <li>Pineto</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>Venerdì</td>
                <td class="table-info-hours">
                    <ul>
                        <li>12-14</li>
                        <li>18-20</li>
                    </ul>
                </td>
                <td>
                    <ul>
                        <li><a href="#mosciano" class="table table-info table-info-place">Mosciano</a></li>
                        <li>Pineto</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>Sabato</td>
                <td class="table-info-hours">
                    <ul>
                        <li>12-14</li>
                        <li>18-20</li>
                    </ul>
                </td>
                <td>
                    <ul>
                        <li><a href="#mosciano" class="table table-info table-info-place">Mosciano</a></li>
                        <li>Pineto</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>Domenica</td>
                <td class="table-info-hours">
                    <ul>
                        <li>12-14</li>
                        <li>18-20</li>
                    </ul>
                </td>
                <td>
                    <ul>
                        <li><a href="#mosciano" class="table table-info table-info-place">Mosciano</a></li>
                        <li>Pineto</li>
                    </ul>
                </td>
            </tr>
        </table>
    </main>
    <div class="modal modal-dark-frame">
    </div>
    <section class="modal modal-info modal-info-hidden modal-info-mosciano">
        <span class="modal-close">X</span>
        <h1>Mosciano</h1>
        <span class="modal modal-info-address">Via Garibaldi 32, Mosciano, 24142, (TE), Italia</span>
        {{-- Includere google maps --}}
        <div class="modal modal-info-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d46979.435161927!2d14.047895849999998!3d42.61440245!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1331bd7b87aadcfd%3A0xf7548649fc8e4f72!2sPineto+TE!5e0!3m2!1sit!2sit!4v1483108878163" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </section>
@endsection