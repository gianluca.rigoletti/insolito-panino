<!DOCTYPE html>
<html>
<head>
    <title>Insolito panino | @yield('title')</title>
    {{-- Meta tags --}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ elixir('/css/app.css') }}">
</head>
<body>
    
    {{-- Inserisco tre parti principali:
    1. Header
    2. Main
    3. Footer  --}}

    @include('header')
    @yield('main')
    @include('footer')
    <script src="{{ elixir('/js/app.js') }}"></script>
</body>
</html>