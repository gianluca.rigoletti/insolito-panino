<header class="header">
    <h4 class="header-title font-cursive">
        <a href="/" class="link-unstyled">Insolito Panino</a>
    </h4>
    <span class="chevron bottom hidden-lg"></span>
    <nav class="nav nav-menu visible-lg">
        <ul class="nav-menu-list">
            <li class="nav-menu-item"><a href="/menu">Menu</a></li>
            <li class="nav-menu-item"><a href="/contatti">Contatti</a></li>
            <li class="nav-menu-item"><a href="/info">Info</a></li>
        </ul>
    </nav>

</header>
{{-- Navbar section clickable --}}
<nav class="navbar-hidden hidden-lg">
    <ul>
        <li><a href="/">Home</a></li>
        <hr class="hidden-lg">
        <li><a href="/menu">Menu</a></li>
        <li><a href="/offerte">Offerte</a></li>
        <hr class="hidden-lg">
        <li><a href="/contatti">Contatti</a></li>
        <li><a href="/info">Orari e Luogo</a></li>
    </ul>
</nav>