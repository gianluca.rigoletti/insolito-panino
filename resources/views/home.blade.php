@extends('app')

@section('main')
    <main class="main">
        <h1>Il miglior panino di Pineto.</h1>
        
        <section class="section-home section-home-description">
            <img src="{{ elixir('/images/burger.svg') }}" class="img">
            <h2>I panini fatti con il ♡</h2>
            <p class="text-home">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.
            </p>
        </section>
        <section class="section-home section-home-food">
            <img src="{{ elixir('/images/french-fries.svg') }}" class="img">
            <h2>Stuzzichini e arrosticini.</h2>
            <p class="text-home">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.
            </p>
        </section>
        <section class="section-home section-home-info">
            <img src="{{ elixir('/images/van.svg') }}" class="img">
            <h2>La base operativa</h2>
            <p class="text-home">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat.
            </p>
        </section>
    </main>
@endsection