@extends('app')

@section('main')
    <main class="main main-menu">
        <h1>Il nostro menu</h1>
        @foreach($foods as $food)
            <section class="food-item"
            data-food-name="{{ $food->name }}"
            data-food-price="{{ $food->price }}"
            >
                <h2 class="food-title" >{{ $food->name }}</h2>
                <figure class="food-figure">
                    <img 
                    class="food-image" 
                    src="{{ elixir('/images/food/'.$food->photo_name) }}" 
                    alt="{{ $food->name }}">
                    <figcaption class="food-info">
                        <h3 class="food-ingredients-header hidden-xs">Ingredienti</h3>
                        <p class="food-text food-ingredients">
                            {{ $food->ingredients }}
                        </p>
                        <h3 class="food-ingredients-header hidden-xs">Prezzo</h3>
                        <span class="food-text food-price" >€ {{ $food->price }}</span>
                    </figcaption>
                </figure>
            </section>
        @endforeach
    </main>
@endsection