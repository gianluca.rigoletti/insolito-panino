<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class FoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::Table('foods')->insert([
            'name' => 'Pulled Pork',
            'price' => 6,
            'type' => 'hamburgers',
            'ingredients' => 'pulled pork, salsa barbeque, insalata,
                                bacon',
            'photo_name' => 'pulled_pork.jpg'            
        ]);
        DB::Table('foods')->insert([
            'name' => 'Arrosticini',
            'price' => 0.5,
            'type' => 'varie',
            'ingredients' => 'carne di pecora',
            'photo_name' => 'arrosticini.jpg'            
        ]);
        DB::Table('foods')->insert([
            'name' => 'Legendary Burger',
            'price' => 8,
            'type' => 'hamburgers',
            'ingredients' => 'pulled pork, capra, cinghiale intero,
                            caccole, vitamine',
            'photo_name' => 'panino_2.jpg'            
        ]);
    }
}
